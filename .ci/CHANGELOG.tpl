{{#releases}}
## [{{name}}](https://gitlab.zenith.igzdev.com/zenith-platform/mlops-aiorchestrator/aio/tags/{{name}}) ({{date}})

{{#sections}}
### {{name}}

{{#commits}}
* [[{{#short5}}{{sha}}{{/short5}}](https://gitlab.zenith.igzdev.com/zenith-platform/mlops-aiorchestrator/aio/commit/{{sha}})] {{message.fullMessage}} ({{authorAction.identity.name}}, {{#timestampISO8601}}{{commitAction.timeStamp.timeStamp}}{{/timestampISO8601}})

{{/commits}}
{{^commits}}
No changes.
{{/commits}}
{{/sections}}
{{^sections}}
No changes.
{{/sections}}
{{/releases}}
{{^releases}}
No releases.
{{/releases}}

